var app = angular.module('myApp',[]);

var reg_button = document.getElementById("reg_button");
app.controller('myCtrl', function($scope,$interval){
    $scope.firstName = 'Matthew';
    $scope.lastName = 'Chuong';
    $scope.theTime = new Date().toLocaleTimeString();
    $interval(function(){
        $scope.theTime = new Date().toLocaleTimeString();
    }, 1000);
    $scope.password ='';
    $scope.num = $scope.password.length;
});

var config = require('./config.json');
firebase.initializeApp(config);